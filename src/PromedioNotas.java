/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class PromedioNotas {

    /*
     Desarrolle un algoritmo que permita calcular el promedio de notas de los exámenes de un estudiante.
El algoritmo solo permite ingresar una nota a la vez.
Considere que se realizan únicamente tres exámenes (nota1, nota2, nota3).

     */
    float promedio, nota;
    int i;
    Scanner ingreso = new Scanner(System.in);

    private void operacionpromedios() {

        i = 1;
        promedio = 0;
        while (i <= 3) {
            System.out.println("Digite la nota "+i);
            nota = ingreso.nextFloat();
            promedio = promedio + nota;
            i = i + 1;
            promedio = promedio / 3;

        }
        System.out.println("El promedio es: "+promedio);

    }

    public void promedio() {

        System.out.println("\nPrograma  que permite calcular el promedio de notas de los exámenes de un estudiante\n");
        operacionpromedios();
    }

}
