/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class NumeroHasta20 {

    /*
    Usando una estructura Mientras (while), realizar un algoritmo que escriba los números de uno en uno hasta 20

     */
    int i;

    private void contador() {
        i = 1;
        while (i <= 20) {

            System.out.println(i);
            i = i + 1;
        }
    }

    public void numeros20() {

        Scanner ingreso = new Scanner(System.in);
        System.out.println("\nPrograma que imprime los números de uno en uno hasta 20\n");
        contador();
    }
}
