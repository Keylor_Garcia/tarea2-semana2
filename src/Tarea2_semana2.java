/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
public class Tarea2_semana2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner ingreso = new Scanner(System.in);
        int opcion;
        /*
        En esta parte va a ir el menú
        */
        System.out.println("Lista de programas\n");
        System.out.print("1.Progra de calcular edad\n");
        System.out.print("2.Progra de salarios\n");
        System.out.print("3.Progra de números hasta el 20\n");
        System.out.print("4.Progra de promedio de notas\n");
        System.out.print("Opción a ejecutar: ");
        opcion = ingreso.nextInt();
        switch (opcion){
            case 1:
                EdadActual datos1 = new EdadActual();
                datos1.edad();
                break;
            case 2:
                Salarios datos2 = new Salarios();
                datos2.salario();
                break;
                
            case 3:
                NumeroHasta20 datos3 = new NumeroHasta20();
                datos3.numeros20();
                break;   
                
            case 4:
                PromedioNotas datos4 = new PromedioNotas();
                datos4.promedio();
                break;     
                
                
            default:
                System.out.printf("Opción no válida\n");
                break; 
                
        }
    }
    
}

