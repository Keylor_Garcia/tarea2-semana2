/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2_semana2;

import java.util.Scanner;

/**
 *
 * @author usuario
 */
/*
Se requiere un algoritmo para calcular la planilla de una persona
Salario bruto, salario neto, teniendo en consideración que al empleado se le deduce el 9.17% de su salario como cargas sociales.
Para ello se dispone de las horas laboradas por mes y el monto por hora.
Debe mostrar el salario bruto, el salario neto y el monto de las deducciones.

 */
public class Salarios {

    int horasLaboradas;
    float precioHora, salarioBruto, salarioNeto, deducciones;

    private void calcularsalario() {

    salarioBruto = horasLaboradas * precioHora;
    salarioNeto = (float) (salarioBruto - (salarioBruto * (9.17 / 100)));
    deducciones = salarioBruto - salarioNeto;

        System.out.println("El salario buto es: "+salarioBruto+", el salario neto es: "+salarioNeto+", las deducciones son: "+deducciones);
        
    }
        
    public void salario() {

        Scanner ingreso = new Scanner(System.in);
        System.out.println("\nPrograma que calcula la planilla de una persona\n");
        System.out.print("\nDigite la cantidad de horas laboradas en el mes: ");
        horasLaboradas = ingreso.nextInt();

        System.out.print("\nDigite el precio por hora: ¢");
        precioHora = ingreso.nextInt();
        calcularsalario();

    }
}
